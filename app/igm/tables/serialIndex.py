from .mtable import MTable
from .mcolumn import MColumn


class SerialIndexInfo(MTable):
    def __init__(self):
        self.table_id = 'MyDataGrid2'
        self.columns = [
            MColumn(0, 'port', 'Port'),
            MColumn(1, 'igmNumber', 'IGM No', 'int'),
            MColumn(2, 'year', 'Year', 'int'),
            MColumn(3, 'indexNumber', 'Index No', 'int'),
            MColumn(4, 'indexSerialNumber', 'Index Srl No', 'int'),
            MColumn(5, 'itemDescription', 'Item Desc'),
            MColumn(6, 'quantity', 'Quantity', 'float'),
            MColumn(7, 'unit', 'Unit'),
            MColumn(8, 'packages', 'Packages'),
            MColumn(9, 'unit', 'Unit'),
        ]
        self.output_file = 'data/{session}/{port}/serial_index_info.csv'
