from .mtable import MTable
from .mcolumn import MColumn


class HouseIndexInfo(MTable):
    def __init__(self):
        self.table_id = 'DataGrid11'
        self.columns = [
            MColumn(0, 'port', 'Port'),
            MColumn(1, 'igmNumber', 'IGM No', 'int'),
            MColumn(2, 'year', 'Year', 'int'),
            MColumn(3, 'indexNumber', 'Index No', 'int'),
            MColumn(4, 'blNumber', 'BL No',),
            MColumn(5, 'blDate', 'BL Date', 'date'),
            MColumn(6, 'importerName', 'Importer Name'),
            MColumn(7, 'importerAddress', 'Importer Address'),
            MColumn(8, 'consignorName', 'Consignor Name'),
            MColumn(9, 'weight', 'Weight Wt', 'float'),
            MColumn(10, 'shed', 'Shed'),
        ]
        self.output_file = 'data/{session}/{port}/house_index_info.csv'
