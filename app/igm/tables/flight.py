from .mtable import MTable
from .mcolumn import MColumn


class FlightInfo(MTable):
    def __init__(self):
        self.table_id = 'MyDataGrid'
        self.columns = [
            MColumn(0, 'port', 'Port'),
            MColumn(1, 'igmNumber', 'IGM No', 'int'),
            MColumn(2, 'year', 'Year', 'int'),
            MColumn(3, 'flightNumber', 'Flight No'),
            MColumn(4, 'airLine', 'Air Line'),
            MColumn(5, 'departureAirport', 'Departure Airport'),
            MColumn(6, 'arrivalDate', 'Arrival Date', 'date'),
            MColumn(7, 'igmDate', 'IGM Date', 'date')]
        self.output_file = 'data/{session}/{port}/flight_info.csv'
