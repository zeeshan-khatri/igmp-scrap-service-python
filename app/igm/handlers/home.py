from ..tables.flight import FlightInfo
from ..tables.consoleIndex import ConsoleIndexInfo
from ..tables.houseIndex import HouseIndexInfo
from ..tables.serialIndex import SerialIndexInfo
from ..data.home_response import HomeResponseData
from bs4 import BeautifulSoup
from csv import writer
import os
import errno



class HomeRequestHandler():

    def __init__(self, session, response):
        html_doc = response.text
        self.session = session
        self.soup = BeautifulSoup(html_doc, features='html.parser')

    def handle(self):
        viewstate = self.soup.find(id='__VIEWSTATE')['value']
        eventvalidation = self.soup.find(id='__EVENTVALIDATION')['value']
        ports = list(map(lambda port: port['value'], self.soup.find(
            id='PORTCD').select('option')))

        data = HomeResponseData(viewstate=viewstate,
                                eventvalidation=eventvalidation, ports=ports)
        for port in ports:
            self.make_csv(FlightInfo(), port)
            self.make_csv(ConsoleIndexInfo(), port)
            self.make_csv(HouseIndexInfo(), port)
            self.make_csv(SerialIndexInfo(), port)

        return data

    def make_csv(self, table, port):
        filename = table.output_file.format(session=self.session, port=port.strip())

        if not os.path.exists(os.path.dirname(filename)):
            try:
                os.makedirs(os.path.dirname(filename))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

        with open(filename, 'w') as csv_file:
            csv_writer = writer(csv_file)
            csv_writer.writerow(map(lambda x: x.caption, table.columns))
