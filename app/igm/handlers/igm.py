from bs4 import BeautifulSoup
from csv import writer
import pymongo
import os
import errno
from datetime import datetime

from ..tables.flight import FlightInfo
from ..tables.consoleIndex import ConsoleIndexInfo
from ..tables.houseIndex import HouseIndexInfo
from ..tables.serialIndex import SerialIndexInfo


class IGMRequestHandler():

    def __init__(self, session, response, port, igm_number, year):
        html_doc = response.text
        self.session = session
        self.soup = BeautifulSoup(html_doc, features='html.parser')
        self.port = port
        self.igm_number = igm_number
        self.year = year

    def handle(self):
        flightInfo = FlightInfo()
        flightInfoList = self.scrap_html_table(flightInfo)
        count = len(flightInfoList)

        if count > 0:
            consoleIndex = ConsoleIndexInfo()
            houseIndex = HouseIndexInfo()
            serialIndex = SerialIndexInfo()

            consoleIndexList = self.scrap_html_table(consoleIndex)
            houseIndexList = self.scrap_html_table(houseIndex)
            serialIndexList = self.scrap_html_table(serialIndex)

            self.write_in_csv(flightInfo, flightInfoList)
            self.write_in_csv(consoleIndex, consoleIndexList)
            self.write_in_csv(houseIndex, houseIndexList)
            self.write_in_csv(serialIndex, serialIndexList)

            client = pymongo.MongoClient("mongodb://127.0.0.1:27017/")
            db = client["igmplusdb"]
            
            flightInfoCol = db["flightInfo"]
            consoleIndexInfoCol = db["consoleIndexInfo"]
            houseIndexInfoCol = db["houseIndexInfo"]
            serialIndexInfoCol = db["serialIndexInfo"]

            self.write_in_db(flightInfoCol, flightInfo, flightInfoList)
            self.write_in_db(consoleIndexInfoCol, consoleIndex, consoleIndexList)
            self.write_in_db(houseIndexInfoCol, houseIndex, houseIndexList)
            self.write_in_db(serialIndexInfoCol, serialIndex, serialIndexList)

        return count

    def scrap_html_row(self, row):
        columns = row.select('td')[1:]
        data = list(map(lambda x: x.get_text(), columns))
        data = [self.port, self.igm_number, self.year] + data
        return data

    def scrap_html_table(self, table):
        rows = list()
        html_rows = self.soup.find(id=table.table_id).select('tr')[1:] #ignore first row
        count = len(html_rows)

        if count > 0:
            for html_row in html_rows:
                row = self.scrap_html_row(html_row)
                rows.append(row)

        return rows
    
    def write_in_csv(self, table, rows):
        filename = table.output_file.format(session=self.session, port=self.port.strip())

        if not os.path.exists(os.path.dirname(filename)):
            try:
                os.makedirs(os.path.dirname(filename))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        with open(filename, 'a') as csv_file:
            csv_writer = writer(csv_file)
            for row in rows:
                csv_writer.writerow(row)

    def write_in_db(self, collection, table, rows):
        records = list()
        for row in rows:
            record = {}

            for column in table.columns:
                record[column.name] = self.parseValue(row[column.index], column.dataType)

            # print(record)
            records.append(record)

        if(len(records) > 0):
            result = collection.insert_many(records)
            # print("records inserted: ")
            # print(result)

    def parseDate(self, dateString):
        if(len(dateString) == 6):
            year = int(dateString[0:4])
            month = int(dateString[4:6])
            day = int(dateString[6:8])
            return datetime(year, month, day)
        return None

    def parseValue(self, value, dataType):
        if dataType == 'int':
            return int(value)
        elif dataType == 'float':
            return float(value)
        elif dataType == 'date':
            return self.parseDate(value)
        else:
            return value