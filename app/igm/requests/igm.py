import requests
from .base import BaseRequest
import os


class IGMRequest(BaseRequest):
    def __init__(self):
        self.url = '/igm/IGMview1.aspx'
        self.method = 'POST'

    def send(self, data):
        url = self.get_url()
        params = {
            '__VIEWSTATE': data.viewstate,
            '__EVENTVALIDATION': data.eventvalidation,
            'PORTCD': data.port,
            'IGMNO': data.number,
            'IGMCY': data.year,
            'Button2': 'View',
        }
        response = requests.post(url, data=params)

        filename = "data/{port}_last_response.html".format(port=data.port.strip())
        if not os.path.exists(os.path.dirname(filename)):
            try:
                os.makedirs(os.path.dirname(filename))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

        with open(filename, 'w') as text_file:
            text_file.write(response.text)

        return response
