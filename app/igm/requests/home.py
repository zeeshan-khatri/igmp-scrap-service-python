import requests
from .base import BaseRequest


class HomeRequest(BaseRequest):

    def __init__(self):
        self.url = '/igm/IGMview1.aspx'

    def send(self):
        url = self.get_url()
        return requests.get(url)
