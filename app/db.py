from datetime import datetime
from csv import reader
import pymongo


def run():

    client = pymongo.MongoClient("mongodb://127.0.0.1:27017/")
    db = client["igmplusdb"]

    flightInfo = db["flightInfo"]
    consoleIndexInfo = db["consoleIndexInfo"]
    houseIndexInfo = db["houseIndexInfo"]
    serialIndexInfo = db["serialIndexInfo"]
    scrapingData = db["scrapingData"]

    flightInfo.drop()
    consoleIndexInfo.drop()
    houseIndexInfo.drop()
    serialIndexInfo.drop()
    scrapingData.drop()

    # print(flightInfo.find_one())
    # print(consoleIndexInfo.find_one())
    # print(houseIndexInfo.find_one())
    # print(serialIndexInfo.find_one())

    # def parseDate(dateString):
    #     if(len(dateString) == 6):
    #         year = int(dateString[0:4])
    #         month = int(dateString[4:6])
    #         day = int(dateString[6:8])
    #         return datetime(year, month, day)
    #     return None

    # def parseValue(value, dataType):
    #     if dataType == 'int':
    #         return int(value)
    #     elif dataType == 'float':
    #         return float(value)
    #     elif dataType == 'date':
    #         return parseDate(value)
    #     else:
    #         return value

    # def save(collection, table: MTable):
    #     with open(table.output_file, 'r') as csv_file:
    #         csv_reader = reader(csv_file)
    #         line_count = 0
    #         records = list()
    #         for row in csv_reader:
    #             print(line_count)
    #             if (line_count != 0):
    #                 record = {}

    #                 for column in table.columns:
    #                     record[column.name] = parseValue(row[column.index], column.dataType)

    #                 print(record)
    #                 records.append(record)

    #             line_count += 1

    #         result = collection.insert_many(records)
    #         print("records inserted: ")
    #         print(result)

    # save(flightInfo, FlightInfo())
    # save(consoleIndexInfo, ConsoleIndexInfo())
    # save(houseIndexInfo, HouseIndexInfo())
    # save(serialIndexInfo, SerialIndexInfo())

run()