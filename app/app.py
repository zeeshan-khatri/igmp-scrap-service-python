import time
from datetime import datetime
import threading
import pymongo
import traceback

from .igm.requests.home import HomeRequest
from .igm.handlers.home import HomeRequestHandler
from .igm.requests.igm import IGMRequest
from .igm.handlers.igm import IGMRequestHandler
from .igm.data.igm_request import IGMRequestData


# empty response limit
empty_count_limit = 5

# if site is not responding
retry_count_limit = 5
year = 2018

def startSession():
    session = datetime.now().strftime('%Y%m%d-%H%M-%S-%f')
    return session


def run():
    session = startSession()

    home_request = HomeRequest()
    home_response = home_request.send()
    home_handler = HomeRequestHandler(session, home_response)
    data = home_handler.handle()

    threads = []
    # limited to first four port only
    for port in data.ports:
        thread = threading.Thread(
            name=port, target=scraper, args=(data, port, session))
        threads.append(thread)
        thread.start()
        # scraper(data, port, session)


def scraper(data, port, session):

    client = pymongo.MongoClient("mongodb://127.0.0.1:27017/")
    db = client["igmplusdb"]
    collection = db["scrapingData"]

    query = {"port": port.strip()}
    record = collection.find_one(query)

    if (not record):
        print("Record not found for port: {port}.".format(port=port))
        record = {}
        record["port"] = port.strip()
        record['missings'] = []
        record['last'] = 0

        record['_id'] = collection.insert_one(record)
        print("Inserted new record for port: {port}.".format(port=port))
        # print(record)
    else:
        print("Record found for port: {port}.".format(port=port))
        # print(record)
    
    scrap_missing(data, port, year, session, record, collection)
    scrap_from(data, port, year, session, record, collection)
    print("Scraping finished for port: {port}".format(port=port))


def scrap_missing(data, port, year, session, record, collection):
    print("Scraping missings for port: {port}".format(port=port))

    _id = record['_id']
    missings = record['missings']
    if(not missings):
        missings = []

    empty_count = 0
    retry_count = 0
    for missing in missings[:]: # this will make copy of missings instead using original so we can remove from missings
        igm_number = missing

        try:
            count = scrap_igm(data, port, igm_number, year, session)

            if count == 0:
                print(port, igm_number, year, '- still missing')
            else:
                missings.remove(igm_number)
                print("missings: ", missings)
                update = {'$set': {'missings': missings}}
                collection.update_one({'_id': _id}, update)
                print(port, igm_number, year, '- completed')

        except Exception as e:
            print("--------------------------------")
            print(port, igm_number, year, '- error')
            traceback.print_exc()
            retry_count = retry_count + 1
            print('request failed. retry:{0}'.format(retry_count))
            print("--------------------------------")

        time.sleep(1)


def scrap_from(data, port, year, session, record, collection):
    _id = record['_id']
    igm_number = record['last'] + 1
    missings = record['missings']
    if(not missings):
        missings = []
    
    print("Scraping further for port: {port} from igm: {igm}".format(port=port, igm=igm_number))
    empty_count = 0
    retry_count = 0
    while (empty_count < empty_count_limit and retry_count < retry_count_limit):
        try:
            count = scrap_igm(data, port, igm_number, year, session)

            if count == 0:
                empty_count = empty_count + 1
                missings.append(igm_number)
                print("missings: ", missings)
                print("last: ", igm_number)
                update = {'$set': {'missings': missings}}
                collection.update_one({'_id': _id}, update)
                update = {'$set': {'last': igm_number}}
                collection.update_one({'_id': _id}, update)
                print(port, igm_number, year, '- missing')
            else:
                print("last: ", igm_number)
                update = {'$set': {'last': igm_number}}
                collection.update_one({'_id': _id}, update)
                print(port, igm_number, year, '- completed')
                empty_count = 0

            igm_number = igm_number + 1
            retry_count = 0
            
        except Exception as e:
            print("--------------------------------")
            print(port, igm_number, year, '- error')
            traceback.print_exc()
            retry_count = retry_count + 1
            print('request failed. retry:{0}'.format(retry_count))
            print("--------------------------------")            
            time.sleep(3)

        time.sleep(1)


def scrap_igm(data, port, igm_number, year, session):
    igm_request = IGMRequest()
    igm_request_data = IGMRequestData()
    igm_request_data.viewstate = data.viewstate
    igm_request_data.eventvalidation = data.eventvalidation
    igm_request_data.port = port
    igm_request_data.number = igm_number
    igm_request_data.year = year

    igm_response = igm_request.send(igm_request_data)
    igm_handler = IGMRequestHandler(session, igm_response, port, igm_number, year)
    count = igm_handler.handle()
    return count
