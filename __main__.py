from app import app
# from app import db


def main():
    app.run()
    # db.run()


# this means that if this script is executed, then
# main() will be executed
if __name__ == '__main__':
    main()